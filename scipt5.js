// Génère un nombre aléatoire entre 1 et 100
var randomNumber = Math.floor(Math.random() * 100) + 1;

// Récupère l'élément de saisie et le message
var guessInput = document.getElementById('guessInput');
var message = document.getElementById('message');

// Vérifie la devinette de l'utilisateur
function checkGuess() {
  var userGuess = parseInt(guessInput.value);

  if (userGuess === randomNumber) {
    message.textContent = 'Bravo, vous avez deviné le bon nombre !';
    message.style.color = 'green';
  } else if (userGuess < randomNumber) {
    message.textContent = 'Le nombre est plus grand.';
    message.style.color = 'red';
  } else if (userGuess > randomNumber) {
    message.textContent = 'Le nombre est plus petit.';
    message.style.color = 'red';
  }

  guessInput.value = '';
}